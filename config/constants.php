<?php
return [
    'redirect_url'            => 'http://127.0.0.1:8000/paymentRecord',
    'api_hit_url'             => 'https://test.instamojo.com/api/1.1/payment-requests/',
    'msg_ser_hit_url'  => 'https://www.fast2sms.com/dev/bulk',
    'sender_id'               => 'FSTSMS',
    'purpose'                 => 'Wallet Top UP',
    'phone'                   => '7675997950',
    'buyer_name'              => 'nithishsingh',
    'send_email'              => false,
    'webhook'                 => 'http://www.dummy/webbook',
    'send_sms'                => false,
    'email'                   => 'foo@example.com',
    'allow_repeated_payments' => true,
    'language'                => 'english',
    'route'                   => 'qt',
    'message'                 => '31621',
    'variables'               => '{BB}',
        
];