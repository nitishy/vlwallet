<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use RealRashid\SweetAlert\Facades\Alert;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::view('/dummy','dummy2');

Route::get('/mobileverification',function()
{
    return view('mobileVerification');
});

Route::get('/profileUpdate',function()
{
    Alert::success('Success Title', 'Success Message');
    return view('profileUpdation');
});

Route::get('/changePassword',function(){
    return view('changePassword');
});

Route::get('/userProfile','User\UserController@userProfile');
Route::post('/updatedetails','User\UserController@updateDetails')->middleware('update_validations');

Auth::routes(['verify' => true]);

Auth::routes();
Route::get('/getRecepientDetails','User\UserController@getRecepientDetails');

Route::get('/home', 'Home\HomeController@index')->name('home');

//Route::middleware(['sessioncheck','cacheclear'])->group(function(){

    Route:: post('/paymentProcess','User\UserController@creatingRequest')
            ->middleware('wallet_topup_validation');

    Route:: get('/paymentRecord','User\UserController@paymentDetails');

        
    Route:: post('/walletTransfer','Transaction\Transactions@walletTransfer')
            ->middleware('mobilenumber_validation','wallet_transfer_validations');

    Route:: get('/sendMoney','ViewIndexes\ViewsController@transferIndex');

    Route:: get('/sessionCheck','User\UserController@sessionCheck');

    Route:: get('/addMoney','ViewIndexes\ViewsController@index');

    Route:: get('/failed','ViewIndexes\ViewsController@failedIndex')->middleware('sessioncheck');

    Route:: get('/otpVerify','ViewIndexes\ViewsController@otpVerifyIndex');

    Route::get('/otpVerification','Auth\MobileVerification@otpValidate');

    Route::view('/mobileVerify','mobileVerification');

    Route::get('/verifyMobile','Auth\MobileVerification@mobileExistCheck');

//});

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/dashboard',function(){
    return view('dashboard');
});

Route::get('/registration',function(){
    return view('registration1');
});

Route::view('/mylogin','mylogin');
