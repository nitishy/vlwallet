<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersData extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::create('user_Details',function(Blueprint $table){
            $table->integer('user_id');
            $table->string('username',20);
            $table->string('firstname',20);
            $table->string('middlename',20)->nullable();
            $table->string('lastname',20);
            $table->date('dob');
            $table->string('address',100);
            $table->string('city',20);
            $table->string('district',20);
            $table->string('state',20);
            $table->mediumInteger('pincode');
            $table->mediumInteger('mobile',10);
            $table->mediumInteger('balance');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_Details');
    }
}
