const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    'resources/vendor/jquery-3.2.1.min.js',
    'resources/vendor/bootstrap-4.1/popper.min.js',
    'resources/vendor/bootstrap-4.1/bootstrap.min.js',
    'resources/vendor/slick/slick.min.js',
    'resources/vendor/wow/wow.min.js',
    'resources/vendor/animsition/animsition.min.js',
    'resources/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js',
    'resources/vendor/counter-up/jquery.waypoints.min.js',
    'resources/vendor/counter-up/jquery.counterup.min.js',
    'resources/vendor/circle-progress/circle-progress.min.js',
    'resources/vendor/perfect-scrollbar/perfect-scrollbar.js',
    'resources/vendor/chartjs/Chart.bundle.min.js',
    'resources/vendor/select2/select2.min.js',
    'resources/js/main.js',
    'resources/js/mycustomjs.js',
 ], 'public/js/master.js');
 
 mix.styles([
    'resources/css/font-face.css',
    'resources/vendor/bootstrap-4.1/bootstrap.min.css',
    'resources/vendor/animsition/animsition.min.css',
    'resources/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css',
    'resources/vendor/wow/animate.css',
    'resources/vendor/css-hamburgers/hamburgers.min.css',
    'resources/vendor/slick/slick.css',
    'resources/vendor/select2/select2.min.css',
    'resources/vendor/perfect-scrollbar/perfect-scrollbar.css',
    'resources/css/theme.css',
    'resources/css/mycustomstyle.css',
    ], 'public/css/master.css');
 