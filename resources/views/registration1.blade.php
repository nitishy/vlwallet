<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ URL::asset('css/registration.css')}}">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.0/animate.compat.css"
        integrity="sha512-5m1+8f8jC4ePfYvS39HmjqsYkkragJZaXV7kfreb5pytmTlDnZgXZ73JlYC0Ui25beVJMWLJq8AzDv4ZeXC9mg=="
        crossorigin="anonymous" />

</head>

<body class="back">
    <div class="color1 wow backInUp" data-wow-iteration="1"></div>
    <div class="color2 wow backInDown" data-wow-iteration="1"></div>
    
    <div class="row content-row">
        <div  class="col-md-4 col-sm-12 register-image"></div>
        <div class="col-md-5 col-sm-12 form-content p-5">
           
            <div class="husu">
                <h1 >Sign Up..</h1>
                <div class="border1"></div>
            </div>
            
            <form class="pt-4"  name="form1" id="form1" method="POST" action="{{ route('register') }}">
              {{csrf_field()}}
              <!--errors-->
              @include('layouts.error')
                <div class="form-row" >
                  <div class="form-group col-md-6">
                    <label for="inputEmail">Email</label>
                    <input type="email" class="form-control " id="inputEmail" placeholder="Email">
                  </div>
                  <div class="form-group col-md-6 ">
                    <label for="inputPassword4">Password</label>
                    <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
                  </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="firstname">First Name</label>
                      <input type="text" class="form-control" id="firstname" placeholder="First name">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="middlename">Middle Name</label>
                      <input type="text" class="form-control" id="middlename" placeholder="Middlename">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="lastname">Last Name</label>
                        <input type="text" class="form-control" id="lastname" placeholder="Lastname">
                      </div>
                </div>

                <div class="form-group">
                  <label for="inputAddress">Address</label>
                  <input type="text" class="form-control" id="inputAddress" placeholder="eg 1-1-15,some x street landmark">
                </div>
                
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="inputCity">City</label>
                    <input type="text" class="form-control" id="City" placeholder="city">
                  </div>
                  <div class="form-group col-md-5">
                    <label for="inputState">State</label>
                    <input type="text" class="form-control" id="State" placeholder="state">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="pincode">Pincode</label>
                    <input type="text" class="form-control" id="Pincode" placeholder="pincode">
                  </div>
                </div>
                <button type="submit" class="btn go-ahead">Go Ahead..</button>
              </form>

        </div>

    </div>






    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"
        integrity="sha512-Rd5Gf5A6chsunOJte+gKWyECMqkG8MgBYD1u80LOOJBfl6ka9CtatRrD4P0P5Q5V/z/ecvOCSYC8tLoWNrCpPg=="
        crossorigin="anonymous"></script>
    <script>
        new WOW().init();
    </script>
     <script src="{{URL::asset('js/validator.js')}}"></script>
    
 

</body>