@extends('layouts.masterLayout')
@section('bodycontent')
<!--start of send Money form-->
<div class="row m-t-25 ">
    <!--first column-->
    <div class="col-sm-6 col-lg-7 p-5 snd-mny">
        <h2>Wallet Transfer</h2>
        <div style="border-bottom: 5px solid #FF8F56;width:30%;" class="mt-3"></div>
        <!--error div-->
         @include('layouts.error')

        <!--end of error div-->
        <form id="tranferFunds" method="post" action="walletTransfer">
            {{ csrf_field() }}
            <div class="row pt-3">
                <div class="form-group col-md-8"><span style="color:red">*</span>
                    <label for="amount" class="add-money-label mt-2">Amount</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="Enter amount" autofocus>
                </div>
            </div>
            <div class="row ">
                <div class="form-group col-md-8"><span style="color:red">*</span>
                    <label for="mobile" class="add-money-label mt-2">Recepient</label>
                    <input id="number" type="text" class="form-control add-money-text" name="mobile" required placeholder="Enter Recepient Mobile ">
                    <label class="warn_label mt-1" style="color:rgb(247, 160, 0);display:none"> <span style="color:red">Note:</span>Check the details in beside section and proceed </label>
                </div>
            </div>
           
            <div class="buttons-group">
                <button type="button" id="check_user" class="btn  btn-lg btn-success mr-3 cust-btn-add-money btn-show">
                    Click to proceed
                </button>
                <button type="submit"  class="btn  btn-lg btn-success  mr-3 cust-btn-add-money btn-hide " disabled>
                    Transfer
                </button>
                <button type="button"  class="btn  btn-lg btn-success  mr-3 cust-btn-add-money btn-cancel ">
                    Cancel
                </button>
            </div>
        </form>
    </div>
    <!--start of second column-->
    <div class="col-sm-6 col-lg-5 p-5 sender-details" id="sender-icon-hide">
        <div class="snd-tmp-div">
            <i class="zmdi zmdi-swap snd-icon"></i>
        </div>
    </div>
    <div class="col-sm-6 col-lg-5 p-5 sender-details1" id="sender-invalid">
        <div class="snd-tmp-div">
            <i class="zmdi zmdi-close-circle snd-icon"></i>
            <h4 style="color:whitesmoke;dispaly:flex;margin:0px auto">User not found</h4>
        </div>
    </div>
        <!--when user enters the details the details are fetched ansd showed in this div-->
    <div class="col-sm-6 col-lg-5 p-5  sender-details-show"  id="sender-details-display">
        <img class="snd-img mt-5" src="images/avatars/default.jpg" id="rece_image" alt="sender profile picture">
        <h4 id="rece_name">Nithish Singh</h4>
        <h4 id="rece_mobile">7675997950</h4>
    </div>

</div>
@endsection