@extends('layouts.masterLayout')
@section('bodycontent')
<!--User Profile Updation Page-->
<div class="row profile-update p-2 mt-5">
    <div class="col-lg-3 col-sm-6 ">
        <center>
            <div class="circle">
                <!-- User Profile Image -->
                <img class="profile-pic" src="./images/Nithish.png">

                <!-- Default Image -->
                <!-- <i class="fa fa-user fa-5x"></i> -->
            </div>
            <div class="p-image">
                <i class="fa fa-camera upload-button"></i>
                <form>
                    <input class="file-upload" type="file" accept="image/*" />
                </form>
            </div>
            <button type="submit" class="btn  btn-lg btn-success mt-5 mb-3">
                Update
            </button>
        </center>

    </div>
    <!--second col-->
    <div class="col-lg-8 ml-5 mt-5 p-5" style="background:whitesmoke;border-radius:30px">
        <h2>Update Details</h2>
        <div style="border-bottom: 5px solid #FF8F56;width:30%;" class="mt-3"></div>
        <form>
            <div class="row mt-3">
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">Name</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">First Name</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">Middle Name</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">Last Name</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name">
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">Address</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">State</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">City</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">District</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">Pincode</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">Date of Birth</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
            </div>
            <button type="submit" class="btn  btn-lg btn-success mt-5 mb-3">
                Update Details
            </button>
        </form>
    </div>

</div>
@endsection