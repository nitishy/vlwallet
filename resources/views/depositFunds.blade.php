@extends('layouts.masterLayout')
@section('bodycontent')
   <!--start of add monry form -->
<div class="row m-t-25 deposit-fund">
    <div class="col-sm-6 col-lg-5 p-5 add-money-icon">
        <i class="zmdi zmdi-balance-wallet add-icon"></i>
    </div>
    <div class="col-sm-6 col-lg-7 p-5">
        <h2>Deposit Funds To Wallet</h2>
        <div style="border-bottom: 5px solid #68f60c;width:60%;" class="mt-3"></div>
        <br/>
        <!--error div-->
        @include('layouts.error')
        <!-- end of the error div-->
        <form name="deposit" id="deposit" method="POST" action="/paymentProcess">
             {{ csrf_field() }}
            <div class="row pt-3">
                <div class="form-group col-md-6 "><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">Amount</label>
                    <input id="amount" type="text" class="form-control add-money-text" value="" name="amount" required placeholder="Enter amount" autofocus>
                    <span style="color:red;display:none" id="amount_span">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
            </div>
            <button type="submit" class="btn  btn-lg btn-success mt-3 mb-3 cust-btn-add-money ">
                Add Amount
            </button>
        </form>
    </div>

</div>
  <!--end of the add money form-->
@endsection
