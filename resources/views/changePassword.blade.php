@extends('layouts.masterLayout')
@section('bodycontent')
<!-- Cahnge Password page-->

<div class="row pass mt-5">
    <div class="col-sm-6 col-lg-4  p-5 change-pass">
        <div class="snd-tmp-div">
            <i class="zmdi zmdi-lock lock-icon wow wobble " data-wow-iteration="2"></i>
        </div>
    </div>

    <!--column 2-->
    <div class="col-lg-8  p-5" style="background:whitesmoke">


        <div style="border-bottom: 5px solid #FF8F56;width:60%;height:auto" class="pb-3">
            <h2>Change Password</h2>
        </div>
        <form>
            <div class="row mt-3">
                <div class="form-group col-md-12"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">Old password</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-12"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">New Password</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
                <div class="form-group col-md-12"><span style="color:red">*</span>
                    <label for="amount" id="amount_label" class="add-money-label mt-2">Confirm Password</label>
                    <input id="amount" type="text" class="form-control add-money-text" name="amount" required placeholder="name" autofocus>
                    <span style="color:red;display:none" id="amount">&nbsp&nbsp * Amount should be only numbers </span>
                </div>
            </div>
            <button type="button" class="btn btn-lg btn-outline-dark">Change Password</button>

        </form>

    </div>



</div>
@endsection
