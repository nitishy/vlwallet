@if($errors->any())
<div class="row p-3 mt-3 "  style="color:red;width:100%;background:rgb(248, 192, 192)">
    <div class=" col-md-12 "><span style="color:red">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif