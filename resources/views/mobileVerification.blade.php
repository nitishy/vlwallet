<!DOCTYPE html>
<html>

<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
    <link rel="stylesheet" href="{{URL::asset('css/mobileverification.css')}}">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.0/animate.compat.css"
        integrity="sha512-5m1+8f8jC4ePfYvS39HmjqsYkkragJZaXV7kfreb5pytmTlDnZgXZ73JlYC0Ui25beVJMWLJq8AzDv4ZeXC9mg=="
        crossorigin="anonymous" />
  
</head>

<body>

    <div class="container mt-5">
        <div class="row mobile-verify mt-5" >
            <div class="col col-sm-12  col-md-6   mob-icon-verify">
                <div>
                    <h2 style="color:white" class="h2">Get Verify Your Mobile Number</h2>
                    <i class="zmdi zmdi-shield-check tick-mark wow flip" data-wow-iteration="100"
                        style="font-size:60px;"></i>
                </div>
                <div>
                    <i class="zmdi zmdi-smartphone-iphone mob-icon " style="font-size: 400px;"></i>
                </div>
            </div>
            <div class="col col-sm-12 col-md-6 " style="background:rgb(255, 254, 254)">
                <!-- Horizontal Steppers -->
                <div class="bs-stepper mt-5">
                    <div class="bs-stepper-header" role="tablist">
                      <!-- your steps here -->
                      <div class="step" data-target="#logins-part">
                        <button type="button" class="step-trigger">
                          <span class="bs-stepper-circle" id="step1">
                              <span class="number1">1</span>
                              <i class="zmdi zmdi-check" id="icon1" style="font-size:20px;color:white;font-weight:30px;display:none"></i>
                            </span>
                          <span class="bs-stepper-label">Mobile Number</span>
                        </button>
                      </div>
                      <div class="line"></div>
                      <div class="step" data-target="#information-part">
                        <button type="button" class="step-trigger">
                          <span class="bs-stepper-circle " id="step2">
                            <span class="number2">2</span>
                            <i class="zmdi zmdi-check" id="icon2" style="font-size:20px;color:white;font-weight:30px;display:none"></i>
                          </span>
                          <span class="bs-stepper-label">Otp verification</span>
                        </button>
                      </div>
                    </div>
                    <div class="bs-stepper-content">
                      <!-- your steps content here -->
                      <div id="logins-part" class="content" role="tabpanel" aria-labelledby="logins-part-trigger"></div>
                      <div id="information-part" class="content" role="tabpanel" aria-labelledby="information-part-trigger"></div>
                    </div>
                  </div>
                <!-- /.Horizontal Steppers -->
                <form>
                    <!--Enter mobile number field-->
                    <div class="form-row mobile-form p-3" style="justify-content: center;">
                        <div class="col-md-12 mb-3" style="text-align:center">
                            
                          <label for="mobile" class="mob-form-label mt-0 mb-3" style="color:#ff6ec4;font-weight:30px">Mobile Number</label>
                          <div class="mt-3">
                                <input id="0" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                <input id="1" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                <input id="2" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                <input id="3" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                <input id="4" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                <input id="5" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                <input id="6" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                <input id="7" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                <input id="8" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                <input id="9" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                          </div>
                          <div id="mobile-exists" class="mt-4" style="display:none">
                             <span style="color:red">Mobile Number Already Exists....</span><br/>
                             <a href="/login">It seems you are already a user Click this link to login</a>
                          </div>
                          <div class="hide-error" id="number-error">
                            *mobile number must be numeric <br/>
                            *Mobile must have 10 digits
                          </div>
                          <button type="button" class="btn btn-outline-success mt-5" id="mobileclick">Click to proceed.....</button>
                        </div>
                    </div>
                    <!--end of mobile number-->
                    <!--Otp verification-->
                    <div class="form-row otp-verify" style="justify-content: center;display:none;">
                        <div class="col-md-12 mb-3" style="text-align:center">
                        
                          <div  style="color:green;display:block">
                             Otp sent successfully......<br/>
                             Please check your mobile for 4 digits
                          </div>
                          <label for="mobile" class="mob-form-label mt-5 mb-3" style="color:#ff6ec4;font-weight:30px">Verify otp</label>
                          <div>
                            <input id="v0" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                            <input id="v1" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                            <input id="v2" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                            <input id="v3" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                          </div>
                          
                          <div class="invalid-otp mt-3" id="number-error" style="color:red;display:none">
                                Invali Otp......
                          </div>
                          <div class="resend-otp mt-3" id="number-error" style="color:green;display:none">
                            Resent Otp successfully......
                          </div>

                          <button type="button" class="btn btn-outline-success mt-3 mr-3" id="Resendclick">Resend Otp</button>
                          <button type="button" class="btn btn-outline-success mt-3" id="Verifyclick">Verify Otp</button>
                        </div>
                    </div>
                    <!--end of otp verification-->    
                </form>
                <!--Start of the verified iconing-->
                <div class="form-row otp-verified mt-5" style="justify-content: center;display:none;">
                    <div class="col-md-12 mb-3 " style="text-align:center">
                        <i class="zmdi zmdi-check-circle" style="font-size: 200px;color:green"></i>
                        <h5>Account Verification Completed</h5>
                        <a href="/home">Click here to proceed....</a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>






    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"
        integrity="sha512-Rd5Gf5A6chsunOJte+gKWyECMqkG8MgBYD1u80LOOJBfl6ka9CtatRrD4P0P5Q5V/z/ecvOCSYC8tLoWNrCpPg=="
        crossorigin="anonymous"></script>
    <script>
        new WOW().init();
    </script>
    <script>
        $(document).ready(function () {
            var mobilenumber="";
            var otpnumber="";
            function sendOtp(mobilenumber)
            {

                $.ajax({
                    url:"verifyMobile",
                    method:'GET',
                    data:{mobile:mobilenumber},
                    dataType:'text',
                    success:function(data){
                        if(data=="otpsent"){

                            $('#number-error').addClass('hide-error').removeClass('show-error');
                            $('.mobile-form,.number1').css({"display":"none"});
                            $('#step1').css({"background":"green"})
                            $('.otp-verify,#icon1').css({"display":"block"});
                            $('#step2').css({"background":"blue"});
                        }
                        if( data=="exists"){
                            $('#mobile-exists').css({"display":"unset"});
                        }
                        if( data == "sendfailed"){
                            alert("Failed to send otp....");
                        }
                    }
                });
            };

            function verifyOtp(otpnumber)
            {
                $.ajax({
                    url:"otpVerification",
                    method:'GET',
                    data:{otp:otpnumber},
                    dataType:'text',
                    success:function(data){
                        if(data=="matched"){
                            $('.number2,.otp-verify').css({"display":"none"});
                            $('#step2').css({"background":"green"});
                            $('#icon2,.otp-verified').css({"display":"block"});
                        }
                        if( data=="notmatched"){
                            $('.invalid-otp').css({"display":"block"});
                        }
                    }
                });

            }
            
            $('#mobileclick').click(function(){
                mobilenumber="";
                for(i=0;i<10;i++)
                {
                    var id='#'+i;
                    mobilenumber=mobilenumber+$(id).val();
                }
                if(mobilenumber.length !=10) {
                    $('#number-error').addClass('show-error').removeClass('hide-error');
                } else {
                    sendOtp(mobilenumber);   
                }
            })
            $('#Verifyclick').click(function(){

                otpnumber="";
                for(i=0;i<4;i++)
                {
                    var id='#v'+i;
                    otpnumber=otpnumber+$(id).val();

                }
                if(otpnumber.length !=4) {
                    $('.invalid-otp').css({"display":"block"});
                } else {

                    verifyOtp(otpnumber);
                }
            })
     
    })
    </script>
    <script>
        $(function() {
                'use strict';

                var body = $('.otp-verify');
                var mobilefield=$('.mobile-form');

                function goToNextInput(e) 
                {
                    var key = e.which,
                    t = $(e.target),
                    sib = t.next('input');
                    if (key != 9 && (key < 48 || key > 57)) {
                    e.preventDefault();
                    return false;
                    }
                    if (key === 9) {
                    return true;
                    }
                    if (!sib || !sib.length) {
                    sib = body.find('input').eq(0);
                    }
                    sib.select().focus();
                }

                function onKeyDown(e) {
                    var key = e.which;

                    if (key === 9 || (key >= 48 && key <= 57)) {
                    return true;
                    }

                    e.preventDefault();
                    return false;
                }
                
                function onFocus(e) {
                    $(e.target).select();
                }

                body.on('keyup', 'input', goToNextInput);
                body.on('keydown', 'input', onKeyDown);
                body.on('click', 'input', onFocus);
                mobilefield.on('keyup', 'input', goToNextInput);
                mobilefield.on('keydown', 'input', onKeyDown);
                mobilefield.on('click', 'input', onFocus);

                })
    </script>
</body>

</htm>