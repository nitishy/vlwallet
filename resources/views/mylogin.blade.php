<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="{{URL::asset('css/master.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/login.css')}}">
</head>
<body class="login-background">
     <div class="row log-img">
         <div class="col col-sm-12 col-md-5 log-col1 show">
             
             <h2>Welcome Back..</h2>
             <p>to keep connected with us please go ahead and login with your account details....
             We are waiting for you at dashboard...</p>
             <button type="button" id="knowmore" class="btn know-more">Know Us More..</button>
         </div>
         <div class="col-md-5 log-col2">
             <h1>Sign In</h1>
             <div class="dummy-line"></div>
             <form  method="POST" action="{{ route('login') }}">
              {{csrf_field()}}
                <div class="form-row mt-3">
                  <div class="form-group col-md-9">
                    <div class="form-lab">
                    <label for="inputEmail4" class="lab">Email</label>
                    </div>
                    <div class="input-field">
                    <i class="zmdi zmdi-email log-icon" style="color:green;position:absolute"></i>
                    <input type="email"name="email" class="form-control log-input" id="inputEmail4" value="{{ old('email') }}" required>
                    </div>
                  </div>
                </div>
                <div class="form-row mt-1 mb-3">
                  <div class="form-group col-md-9">
                    <div class="form-lab">
                      <label for="password" class="lab">Password</label>
                    </div>
                    <div class="input-field">
                    <i class="zmdi zmdi-email log-icon" style="color:green;position:absolute"></i>
                    <input type="password" name="password" class="form-control log-input" id="password" autocomplete="current-password" required>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-success btn-lg">Sign in</button>
              </form>
         </div>
     </div>
    <script src="{{URL::asset('js/master.js')}}"></script>
</body>