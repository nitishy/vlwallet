@extends('layouts.masterLayout')

@section('bodycontent')

<div class="row m-t-25 pt-5 bal-div">
    <div class="col-sm-6 col-lg-12">
        <div>
            <i class="zmdi zmdi-balance-wallet bal-icon wow flip"></i>
        </div>
        <div>
            <h2 class="bal-font">Wallet Balance</h2>

            <h3 class="pb-3 rs">
                <i class="fa fa-inr rs-icon pt-3" aria-hidden="true"></i>
                &nbsp; <span class="rs-text bal-font mb-5"> 8000</span> &nbsp;
            </h3>
        </div>

    </div>
</div>
<div class="row m-t-25 pt-5">
    <div class="col-sm-6 col-lg-4 wow bounceInLeft">
        <div class="overview-item overview-item--c1">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-money-box"></i>
                    </div>
                    <div class="text pt-3">
                        <h2>Deposit Funds</h2>
                        <button type="button" class="btn  mt-3 mb-3 cust-btn">
                            Deposit Funds
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-4 wow bounceInUp">
        <div class="overview-item overview-item--c2">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-swap"></i>
                    </div>
                    <div class="text pt-3">
                        <h2>Transfer Funds</h2>
                        <button type="button" class="btn  mt-3 mb-3 cust-btn2">
                            Wallet Transfer
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-4 wow bounceInRight">
        <div class="overview-item overview-item--c3">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-book-image"></i>
                    </div>
                    <div class="text pt-3">
                        <h2>Statements &nbsp; &nbsp;</h2>
                        <button type="button" class="btn  mt-3 mb-3 cust-btn3">
                            Account Statement
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-4 wow bounceInLeft">
        <div class="overview-item overview-item--c4">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-upload"></i>
                    </div>
                    <div class="text pt-3">
                        <h2>Debit Transactions</h2>
                        <button type="button" class="btn  mt-3 mb-3 cust-btn4">
                            Debits
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-4 wow bounceInUp">
        <div class="overview-item overview-item--c5">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-download"></i>
                    </div>
                    <div class="text pt-3">
                        <h2>Credit Transactions</h2>
                        <button type="button" class="btn  mt-3 mb-3 cust-btn5">
                            Credits
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-4 wow bounceInRight">
        <div class="overview-item overview-item--c6">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-assignment"></i>
                    </div>
                    <div class="text pt-3">
                        <h2>Last 10 Transactions</h2>
                        <button type="button" class="btn  mt-3 mb-3 cust-btn">
                            Lastest transactions
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end of the cards i.e.., the transactions cards-->


@endsection