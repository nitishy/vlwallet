<!Doctype html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Email verfication</title>
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1 class="display-3">Email Verification</h1>
        <p class="lead">Before proceeding, please check your email for a verification link...</p>
        <hr class="my-4">
        <p>If you did not receive the email</p>
        <br/>
        <br/>
        <p>
        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                <h3>A fresh verification link has been sent to your email address.</h3>
            </div>
            @endif
            </p>
            <p class="lead">
            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <button type="submit" class="btn btn-link p-0 m-0 align-baseline">click here to request another</button>.
            </form>
            </p>
    </div>

</body>

</html>

