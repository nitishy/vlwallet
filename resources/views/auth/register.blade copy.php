<!Doctype html>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <link rel="stylesheet" href=""
<style>
    .error{
        color:red;
    }
</style>
</head>
<body class="hold-transition register-page" style="height:auto;overflow:scroll;background:rgb(54, 63, 77)">
<div style="margin-top:4%"></div>
    <!-- general form elements -->
    <div class="card card-primary" style="width:80%">
              <div class="card-header bg-success">

                 <br/>
                <h1 class="card-title" style="font-size:2em;">Registration</h1>
                <br/>
                <br/>

              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form name="registration" id="form1" method="POST" action="{{ route('register') }}">
                {{csrf_field()}}
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6" style="color:red"><span style="color:red">
                            @if($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div>*  {{ $error }}</div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                <div class="row">
                <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="name" id="name_label">Name</label>
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus >
                  </div>
                  <div class="form-group  col-md-6"><span style="color:red">*</span>
                    <label for="email" id="email_label">Email address</label>
                    <input id="email" type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required autocomplete="email">

                  <span style="color:red;display:none" id="email_length_span">&nbsp&nbsp * Email should not be empty</span>
                    <span style="color:red;display:none" id="email_format_span">&nbsp&nbsp * Email must be in standard format</span>
                  </div>
                </div>
                <!--name section-->
                <div class="row">
                <div class="form-group col-md-4"><span style="color:red">*</span>
                    <label for="firstname" id="firstname_label">first name</label>
                    <input type="text" class="form-control" id="firstname" name="firstname" value="{{ old('firstname') }}" placeholder="Enter first name"  required/>

                </div>
                  <div class="form-group  col-md-4"><span style="color:red">*</span>
                    <label for="middlename" id="middelname_label">middle name</label>
                    <input type="text" class="form-control" id="middlename" name="middlename" value="{{ old('middlename') }}" placeholder="Enter middle name" required/>

                  </div>
                  <div class="form-group  col-md-4"><span style="color:red">*</span>
                    <label id="lastname_label">last name</label>
                    <input type="text" class="form-control" id="lastname" name="lastname" value="{{ old('lastname') }}" placeholder="Enter Lastname"  required/>
                  </div>
                </div>
                <!--end of name section-->
                <div class="row">
                  <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="password" id="passwrd_label">Password</label>
                    <input type="password" id="password" class="form-control" id="password" name="password" placeholder="Password"  required/>

                  </div>
                  <div class="form-group col-md-6"><span style="color:red">*</span>
                    <label for="confirm_password" id="confirm_password_label">Confirm Password</label>
                    <input type="password" id="password_confirmation" class="form-control" data-rule-equalTo="#password"  name="password_confirmation" placeholder="Confirm password"  required/>

                  </div>
                </div>


                <!-- Date range -->
                <div class="form-group"><span style="color:red">*</span>
                  <label for="date_of_birth" id="date_of_birth_label">Date of birth:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="date" class="form-control" id="date_of_birth" value="{{ old('date_of_birth') }}" name="date_of_birth"  required/>

                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
            <div class="row">

                                <div class="form-group col-md-6">
                                    <label for="address" id="address1_label"><span style="color:red">*</span>Address-1</label>
                                    <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" placeholder="Your place"  required/>

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="city" id="city1_label"><span style="color:red">*</span>City/Town</label>
                                    <input type="text" class="form-control" id="city1" name="city" value="{{ old('city') }}" placeholder="Your city"  required/>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="district" id="district1_label"><span style="color:red">*</span>District</label>
                                    <input type="text" class="form-control" id="district" value="{{ old('district') }}" name="district" placeholder="Your district"  required/>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="state" id="state1_label"><span style="color:red">*</span>State</label>
                                    <input type="text" class="form-control" id="state"  value="{{ old('state') }}"name="state" placeholder="Your state" required/>
                                                                  </div>

                                <div class="form-group col-md-6">
                                    <label for="pincode" id="pincode1_label"><span style="color:red">*</span>Pincode</label>
                                    <input type="text" class="form-control" id="pincode"  value="{{ old('pincode') }}"name="pincode" placeholder="Your pincode"required/>

                                </div>
            </div>
            <!--

             -->
                <!-- /.card-body -->
                <div class="card-footer " style="width:50%;margin:0px auto;">
                 <center> <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button></center>
                </div>
              </form>
            </div>




    <!-- date-range-picker -->
    <script src="{{URL::asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>

    <script src="{{URL::asset('loginassets/js/validations.js')}}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

</body>

</html>
