//register validations

$(function () {
  
  $("form[name='registration']").validate({
    highlight: function highlight(element) {
      $(element).addClass("is-invalid").removeClass('is-valid');
    },
    unhighlight: function unhighlight(element) {
      $(element).addClass("is-valid").removeClass('is-invalid');
    },
    rules: {
      
      firstname: {
        required: true,
        minlength: 3
      },
      middlename: {
        required: true,
        minlength: 3
      },
      lastname: {
        required: true,
        minlength: 3
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 5
      },
      /*
      password_confirmation: {
        equalTo: "#password"
      },
      date_of_birth: {
        required: true //checkYear: true

      },*/
      address: "required",
      state: "required",
      city: "required",
      pincode: {
        required: true,
        exactlength: 6,
        digits: true
      }
    },
    messages: {
    
      firstname: {
        required: "Please enter your firstname",
        minlength: "the minimum length should be 3"
      },
      middlename: {
        required: "Please enter your middlename",
        minlength: "the minimum length should be 3"
      },
      lastname: {
        required: "Please enter your middlename",
        minlength: "the minimum length should be 3"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      /*
      confirm_password: {
        required: "Confirm password is required",
        equalTo: "Password and Confirm password must be same"
      },
      date_of_birth: {
        required: "date of birth compulsory",
        checkYear: "Invalid Age "
      },*/
      address: "please enter the address",
      state: "please enter the state",
      city: "please enter the city ",
      pincode: {
        required: "please entr the pincode",
        digits: "only digits are allowed",
        exactlength: "Please enter a valid pincode "
      },
      email: "Please enter a valid email address"
    },
    //only submitted when the form is valid
    submitHandler: function submitHandler(form) {
      form.submit();
    }
  });
});

//deposit validations

$(function () {

  jQuery.validator.addMethod("exactlength", function(value, element, param) {
    return this.optional(element) || value.length == param;
   }, $.validator.format("Please enter exactly 10 characters."));


  jQuery.validator.addMethod("minimum_amount", function(value, element, param) {
    return this.optional(element) || parseInt(value) > param;
   }, $.validator.format("Please enter exactly 10 characters."));

   //deposit form validations 
    $("#deposit1").validate({
      highlight: function highlight(element) {
        $(element).addClass("is-invalid").removeClass('is-valid');
      },
      unhighlight: function unhighlight(element) {
        $(element).addClass("is-valid").removeClass('is-invalid');
      },
      rules: {
        amount: {
          required: true,
          digits: true,
          minlength:2,
          minimum_amount:09,
        },
      },
      messages: {

        amount:{

            required : "Amount is compulsory",
            digits: "Amount should be numeric value",
            minlength:"Minimum amount is 10",
            minimum_amount:"Minimum amount to process request is 10"

        },       
      },
      //only submitted when the form is valid
      submitHandler: function submitHandler(form) {
        form.submit();
      }
    });
//Tranfer form validator
 

  $("#tranferFunds").validate({
    highlight: function highlight(element) {
      $(element).addClass("is-invalid").removeClass('is-valid');
    },
    unhighlight: function unhighlight(element) {
      $(element).addClass("is-valid").removeClass('is-invalid');
    },
    rules: {
      amount: {
        required: true,
        digits: true,
        minlength:2,
        minimum_amount:09,
      },
      mobile: {
        required: true,
        digits: true,
        exactlength: 10,
      }
    },
    messages: {

      amount:{

          required : "*Amount is compulsory",
          digits: "*Amount should be numeric value",
          minlength:"Minimum amount is 10",
          minimum_amount:"Minimum amount to process request is 10"


      },
      mobile:{

        required : "*Recepient Mobile Number is required",
        digits: "*Mobile number must be Numeric",
        exactlength: "*Mobile number is invalid"
    }

     
    },
    //only submitted when the form is valid
    submitHandler: function submitHandler(form) {
      $('#number').prop("disabled",false);
      form.submit();
    }
  });
});
