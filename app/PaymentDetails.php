<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;
use PhpParser\Node\Expr\Cast\Bool_;

class PaymentDetails extends Model
{
    /**
     * 
     *stores the table name
     * 
     * @var string
     */
    protected $table        = "paymentDetails";
    /**
     * Tells timestamps present in tabel or not
     *
     * @var boolean
     */
    public    $timestamps   = false;
    /**
     * Stores the  Primary key
     *
     * @var string
     */
    protected $primaryKey   = 'user_Id';

    /**
     * Function that records payment details
     *
     * @param Request $request
     * @param integer $user_Id
     * 
     * @return Bool
     */
    public function recordPayment(Request $request, int $user_Id): Bool
    {
        $currentDateTime              = Carbon::now();
        $record                       = new PaymentDetails;
        $record->user_Id              = $user_Id;
        $record->payment_Id           = $request->payment_id;
        $record->payment_Request_Id   = $request->payment_request_id;
        $record->to                   = $user_Id;
        $record->from                 = $user_Id;
        $record->date_And_Time        = $currentDateTime;
        $record->payment_Status       = $request->payment_status;
        $record->save();
        
        return true;
    
    }
}
