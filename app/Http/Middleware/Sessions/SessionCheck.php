<?php

namespace App\Http\Middleware\Sessions;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;

class SessionCheck
{
    /**
     * Function to handle request and redirect it to dashboard
     *
     * @param Request $request
     * @param Closure $next
     * 
     * @return 
     */
    public function handle(Request $request, Closure $next)
    {
        if(Session::has('user_Id')) {
            return $next($request);
        
        }
        return redirect('/login');

    }
    public function terminate($request,$response)
    {
        Session::put('test2','working');
    }
}
