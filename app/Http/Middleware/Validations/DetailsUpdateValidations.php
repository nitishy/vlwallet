<?php

namespace App\Http\Middleware\Validations;

use Closure;

class DetailsUpdateValidations
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
        $validatedData = $request->validate( [
            'name'             => ['required', 'string', 'max:255','min:6'],
            'firstname'        => ['required',' min:3 '],
            'middlename'       => ['required','min:4 '],
            'lastname'         => ['required ',' min:4 '],
            'address'         => ['required'],
            'city'            => ['required'],
            'district'        => ['required'],
            'state'           => ['required'],
            'pincode'         => ['required','digits_between:6,6 '],
            'avatar'          =>['nullable','image','mimes:jpg,jpeg,png,bmp']
        ]);
        return $next($request);
    }
}
