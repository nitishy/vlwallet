<?php

namespace App\Http\Middleware\Validations;

use Closure;
use Illuminate\Http\Request;

class MobileNumberValidation
{
    /**
     * Function to handle the request to check the validation of mobile number
     *
     * @param Request $request
     * @param Closure $next
     * 
     * @return response
     */
    public function handle( Request $request, Closure $next)
    {
        $validatedData = $request->validate([
            'mobile' => ['required','min:10','max:10'],
        ]);
        if ($request->has('amount')) {

            if( (int)$request->amount < 10)
            return redirect()->back()->withErrors(['Invalid_amount'=>'Invalid amount and amount must be Minimum 10 rupees']);
        }
        return $next($request);
    }
}
