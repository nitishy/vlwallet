<?php

namespace App\Http\Middleware\Validations;

use Closure;
use App\UsersData;
use GuzzleHttp\Middleware;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Mixed_;

class WalletTransferValidations
{
    
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * 
     * @return RedirectResponse
     */
    public function handle(Request $request, Closure $next): RedirectResponse
    {
        $user_Id         = session('user_Id');
        $balanceInstance = new UsersData();
        $balanceStatus   = $balanceInstance->nameAndBalance($user_Id);
        $senderMobile    = $balanceInstance->getField($user_Id, 'mobile');
        $recepientStatus = $balanceInstance->checkMobile($request->mobile);
        if($senderMobile == $request->mobile || !$recepientStatus) {

            return redirect()->back()->withErrors(['invalid_mobile'=>'Invalid Mobile Number or User does not exists']);;

        }    
        if (((int)$balanceStatus['balance'] ) < (int)$request->amount) {

            
            return redirect()->back()->withErrors(['insufficient_balance'=>'Insufficient balance..... ']);

        }

        return $next($request);
        
    }

}
