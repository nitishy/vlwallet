<?php

namespace App\Http\Middleware\Validations;

use Closure;

class WalletTopupValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validatedData = $request->validate([
            'amount' => ['required','numeric','min:2'],
        ]);

        return $next($request);
    }
}
