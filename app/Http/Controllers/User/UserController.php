<?php

namespace App\Http\Controllers\User;

use RealRashid\SweetAlert\Facades\Alert;
use App\UsersData;
use App\PaymentDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Function for creating request to api
     *
     * @param Request $request
     * 
     * @return RedirectResponse
     */
    public function creatingRequest(Request $request): RedirectResponse
    {
        $amount = (int)$request->amount;
        Session(['amount'=>$amount]);
        $response= Http::withHeaders([
            'X-Api-Key'    => env('MY_API_KEY'),
            'X-Auth-Token' => env('MY_AUTH_TOKEN')
        ])->post(Config::get('constants.api_hit_url'),[
            'purpose'                 => Config::get('constants.purpose'),
            'amount'                  => $amount,
            'phone'                   => Config::get('constants.phone'),
            'buyer_name'              => Config::get('constants.buyer_name'),
            'redirect_url'            => Config::get('constants.redirect_url'),
            'send_email'              => false,
            'webhook'                 => Config::get('constants.webhook'),
            'send_sms'                => Config::get('constants.send_sms'),
            'email'                   => Config::get('constants.email'),
            'allow_repeated_payments' => true
        ]);
        $data = json_decode($response,true);
        if ($data['success'] == true)
            return redirect($data['payment_request']['longurl']);

        return redirect('/home');
    }
    
    /**
     * Function for paymentDetails
     *
     * @param Request $request
     * 
     * @return RedirectResponse
     */
    public function paymentDetails(Request $request): RedirectResponse
    {
        $user_Id          = session('user_Id');
        $amount           = (int)session('amount');
        $payment_Instance = new PaymentDetails;
        $payment_status   = $payment_Instance->recordPayment($request,$user_Id);
        if ($payment_status) {
            $updateBalance = new UsersData;
            $updateBalance->updateBalance($user_Id,$amount);
            $userdet = new UsersData;
            session()->forget('amount');   
  
            return redirect('/home')->with('success','Wallet TopUp Success....');
        
        } else {
            
            return redirect('/addMoney')->with('error','Wallet TopUp Failed...');
        
        }
    }

    /**
     * function to check whether the user already updated the mobile number or not
     *
     * @return void
     */
    public function sessionCheck(): RedirectResponse
    {

        $user_Id = session('user_Id');
        $result  = UsersData::find($user_Id);
        if ($result->mobile == null) {
            
            return redirect('/mobileVerify');
        
        }
        return redirect('/home');
   }

   /**
     * Function that shows WalletTransfer
     *
     * @param Request $request
     * @return void
     */
    public function walletTransfer(Request $request): RedirectResponse
    {
        $user_Id         = session('user_Id');
        $balanceInstance = new UsersData;
        $status          = $balanceInstance->walletTransfer($user_Id, $request->mobile, $request->amount);
        if ($status) {
            session()->flash('transferSuccess', 'Transfer Successful');

            return redirect('/home');
        } else {
            session()->flash('transferFailed', 'Transfer failed try again ');

            return redirect('/home');
        }
    }
    
    /**
     *  logging out the user
     *
     * @param Request $request
     * 
     * @return RedirectResponse
     */
    public function logoutUser(Request $request): RedirectResponse
    {
        $request->session()->flush();
       
        return redirect('/dashboard'); 
    }


    public function updateDetails(Request $request)
    {
        $updation = new UsersData;
        $updation->updateUserDetails($request);
        if($updation) {
            return redirect('/home')->with('updation','updation successful');
        }
        dump("updation failed");
    }

    public function userProfile()
    {
        return view('/userProfile');
    }
    public function getRecepientDetails(Request $request)
    {
       if($request->ajax())
       {
            $details = UsersData::select('avatar','username','mobile')
                                ->where('mobile', $request->number)
                                ->get();
            if( count($details) == 1 ) {
                $data = $details->toArray();
                return $data;
            }
       }
    }

}

