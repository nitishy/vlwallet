<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UsersData;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Regist $result=User::where('email',$request->email)->get();
        session(['user_Id'=>$result[0]->id]);
        $userData   = UsersData::find($result[0]->id);
        Session(['username'=>$userData->username]);
        Session(['balance'=>$userData->balance]);er Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo ='/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email'            => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'         => ['required', 'string', 'min:8'],
            'firstname'        => ['required',' min:3 '],
            'middlename'       => ['required','min:4 '],
            'lastname'         => ['required ',' min:4 '],
            'address'         => ['required'],
            'city'            => ['required'],
            'state'           => ['required'],
            'pincode'         => ['required','digits_between:6,6 '],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        DB::beginTransaction();
        try{
            $email=$data['email'];
            $response=User::create([
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
            $result=User::where('email',$email)->get();
            $table_Instance = new UsersData();
            $table_Instance->user_Id = $result[0]->id;
            $table_Instance->username = $data['firstname'];
            $table_Instance->firstname = $data['firstname'];
            $table_Instance->middlename = $data['middlename'];
            $table_Instance->lastname = $data['lastname'];
            $table_Instance->dob = null;
            $table_Instance->address = $data['address'];
            $table_Instance->city = $data['city'];
            $table_Instance->state = $data['state'];
            $table_Instance->pincode = $data['pincode'];
            $table_Instance->mobile = null;
            $table_Instance->balance = 0;
            $table_Instance->save();
            DB::commit();
            return $response;

        } catch (\Exception $e) {
             DB::rollback();
             dump("registration failed");
        }

    }
}
