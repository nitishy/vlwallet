<?php

namespace App\Http\Controllers\Auth;

use App\UsersData;
use App\userDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Config;

class MobileVerification extends Controller
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function mobileExistCheck(Request $request)
    {
        if($request->ajax()) {
            $mobileInstance = UsersData::select('mobile')
                                        ->where('mobile',$request->mobile)
                                        ->get();
            if (count($mobileInstance)==1) {
                return "exists";
            }
            $status = $this->generateOtp($request);
            if ($status)
                return "otpsent";
            else
                return "sendfailed";
        }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function generateOtp(Request $request)
    {
        $pin    = mt_rand(1000, 9999);
        $mobile = $request->mobile;
        //request process starts here
        $response= Http::withHeaders([
            'authorization' => env('MSG_API_KEY')
        ])->post(Config::get('constants.msg_ser_hit_url'),[
            'sender_id'        => Config::get('constants.sender_id'),
            'language'         => Config::get('constants.language'),
            'route'            => Config::get('constants.route'),
            'numbers'          => $mobile,
            'message'          => Config::get('constants.message'),
            'variables'        => Config::get('constants.variables'),
            'variables_values' => $pin
        ]);
        $data = json_decode($response,true);
        if ($data['return'] == true) {
            Session(['otp'=>$pin]);
            Session(['mobile'=>$mobile]);

            return true;
         }
        
        return false;
    }
    
    public function otpValidate(Request $request)
    {
        if($request->ajax())
        {
            $user_otp = $request->otp;
            if ($user_otp == session('otp')) {
                $user         = UsersData::find(session('user_Id'));
                $user->mobile = session('mobile');
                $user->save();

                return "matched";
            }
            return "notmatched";

        }
    }


}
