<?php

namespace App\Http\Controllers\Transaction;

use App\UsersData;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;

class Transactions extends Controller
{
    
    /**
     * Function that handles wallettransfer
     *
     * @param Request $request
     * 
     * @return RedirectResponse
     */
    public function walletTransfer(Request $request): RedirectResponse
    {
        $user_Id         = session('user_Id');
        $balanceInstance = new UsersData;
        $status          = $balanceInstance->walletTransfer($user_Id, $request->mobile, $request->amount);
        if ($status) {

            return redirect('/home')->with('success', 'Transfer Successful....');;
        
        } else {

            return redirect('/home')->with('error',"Transfer Failed....");
        
        }
    }
    public function showRecepientDetails($request)
    {

    }

}
