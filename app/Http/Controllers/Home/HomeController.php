<?php

namespace App\Http\Controllers\Home;

use App\UsersData;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');

    }

    /**
     * Show the application dashboard.
     *
     * @param  Request $request
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) : View
    {
        $userdata=UsersData::find(Session::get('user_Id'));
        return view('/dashboard')->with('userdata',$userdata);
    }
}
