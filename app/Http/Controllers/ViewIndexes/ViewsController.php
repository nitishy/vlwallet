<?php

namespace App\Http\Controllers\ViewIndexes;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ViewsController extends Controller
{
    /**
     * Displays otp verification ui
     * 
     * @return view
     */
    public function otpVerifyIndex() : View
    {
        return view('otpVerify');
    }
    /**
     * This function shows the UI of WalletTop
     *
     * @return view
     */
    public function index() : View
    {
       return view('/depositFunds');
    }
    
    /**
     * This function shows the UI of wallet transfer
     *
     * @return view
     */
    public function transferIndex(): View
    {
        return view('/transferFunds');
    }
    public function failedIndex()
    {
        if(Session::has('test')) {

            dump(Session::get('test'));
        }
        return view('/failed');
    }
}
