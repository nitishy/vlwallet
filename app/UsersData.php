<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class UsersData extends Model
{
    
     /**
     *Variable stores the  table name
     * 
     * @var string
     */
    protected $table        = 'user_Details';
    
    /**
     * Variable which shows primary key in the table
     *
     * @var string
     */
    protected $primaryKey   = 'user_Id';
    
     /**
     * boolean  which represents the timestamps field existance in table
     *
     * @var boolean
     */
    public $timestamps      = false;
    
    /**
     * Function that creates user details
     *
     * @param integer $user_Id
     * @param request $request
     * 
     * @return Bool
     */
    public function createUsersData(int $user_Id, Request $request): Bool
    {
        $table_Instance                     = new UsersData;
        $table_Instance->user_Id            = $user_Id;
        $table_Instance->username           = $request->name;
        $table_Instance->firstname          = $request->firstname;
        $table_Instance->middlename         = $request->middlename;
        $table_Instance->lastname           = $request->lastname;
        $table_Instance->dob                = $request->date_of_birth;
        $table_Instance->address            = $request->address1;
        $table_Instance->city               = $request->city1;
        $table_Instance->district           = $request->district1;
        $table_Instance->state              = $request->state1;
        $table_Instance->pincode            = $request->pincode1;
        $table_Instance->mobile             = $request->mobile;
        $table_Instance->alternateMobile    = $request->alternate;
        $table_Instance->balance            = 0;
        $table_Instance->save();
        return true;
    }

    /**
     * Function to update bank balance
     *
     * @param integer $user_Id
     * @param string $amount
     * 
     * @return bool
     */
    public function updateBalance(int $user_Id, string $amount) : bool
    {
        $user_Details           = UsersData::select('balance')
                                            ->where('user_Id', $user_Id)
                                            ->get();
        $balance                = (int)$user_Details[0]['balance'];
        $updatedBalance         = $balance+$amount;
        $newInstance            = UsersData::find($user_Id);
        $newInstance->balance   = $updatedBalance;
        $newInstance->save();
        Session::put('balance', $updatedBalance);

        return true;   
    }
    
    /**
     * function to update balance after transfer.
     *
     * @param int $user_Id
     * @param string $amount
     * 
     * @return bool
     */
    public function transferUpdateBalance(int $user_Id, string $amount) : bool
    {
        $user_Details               = UsersData::select('balance')
                                                ->where('user_Id', $user_Id)
                                                ->get();
        $balance                    = (int)$user_Details[0]['balance'];
        if($user_Id == Session::get('user_Id'))
        {
            $updatedBalance         = $balance-$amount;
            $newInstance            = UsersData::find($user_Id);
            $newInstance->balance   = $updatedBalance;
            $newInstance->save();
            Session::put('balance', $updatedBalance);

            return true;

        }
    
        $updatedBalance         = $balance+$amount;
        $newInstance            = UsersData::find($user_Id);
        $newInstance->balance   = $updatedBalance;
        $newInstance->save();

        return true;
    }
    
// transfereing money from one account to other 

    /**
     * Function used for transfereing money from one account to other
     *
     * @param integer $user_Id
     * @param string $recepient_Mobile
     * @param string $amount
     * 
     * @return Bool
     */
    public function walletTransfer(int $user_Id, string $recepient_Mobile, string $amount): Bool
    {
          $recepient    = new UsersData;
          $recepientId  = $recepient->userIdDetails($recepient_Mobile);
          $recepient->transferUpdateBalance($user_Id, $amount);
          $recepient->transferUpdateBalance($recepientId, $amount);

          return true;
    }
    
    /**
     * Function to get the userId
     *
     * @param string $resMob
     * 
     * @return integer
     */
    public function userIdDetails(string $resMob): int
    {
        $result=UsersData::select('user_Id')
                          ->where('mobile',$resMob)
                          ->get();

        return $result[0]['user_Id'];

    }

    /**
     * Function to get data of field from UsersData table
     *
     * @param integer $user_Id
     * @param string $field
     * 
     * @return string
     */
    public function getField(string $user_Id, string $field): string
    {
        $result = UsersData::find($user_Id);
    
        return $result->$field;
    
    }
    
    /**
     * Function to return the username and balance
     *
     * @param integer $user_Id
     * 
     * @return array
     */
    public function nameAndBalance(int $user_Id): array
    {
        $userData=UsersData::find($user_Id);   
        $userdet=['username'=>$userData->username,'balance'=>$userData->balance];
    
        return $userdet;    
    
    }

    /**
     * Function to check the mobile
     *
     * @param string $resMob
     * @return Bool
     */
    public function checkMobile(string $resMob): Bool
    {
        $result=UsersData::select('user_Id')
                          ->where('mobile',$resMob)
                          ->get();
        if (count($result))
           
            return true;
        
        return false;
    
    }
    /**
     * 
     */
    public function updateUserDetails(Request $request)
    {
        DB::beginTransaction();
        try {
            $table_Instance=UsersData::find(Session::get('user_Id'));
            $table_Instance->username           = $request->name;
            $table_Instance->firstname          = $request->firstname;
            $table_Instance->middlename         = $request->middlename;
            $table_Instance->lastname           = $request->lastname;
            $table_Instance->dob                = $request->date_of_birth;
            $table_Instance->address            = $request->address;
            $table_Instance->city               = $request->city;
            $table_Instance->district           = $request->district;
            $table_Instance->state              = $request->state;
            $table_Instance->pincode            = $request->pincode;
            if($request->hasFile('avatar')){
                $avatar=$request->file('avatar');
                $filename=time().'.'.$avatar->getClientOriginalExtension();
                $destinationPath = public_path('/images/avatars');
                $avatar->move($destinationPath, $filename);
                $table_Instance->avatar=$filename;
            }
            
        $table_Instance->save();
        DB::commit();

        return true;

        } catch (\Exception $e) {
            DB::rollback();

            return false;

        }
    }
}
