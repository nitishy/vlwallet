<?php

namespace Tests\Feature;

use App\User;
use App\UsersData;
use Tests\TestCase;
use CreateUsersTable;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testBasicTest()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    /** @test */
    public function only_logged_in_users_sees_dashboard()
    {
        //here directly accessing home page without login
        $response=$this->get('/home')->assertRedirect('/login');
    }

    /** @test */
    public function if_user_is_logged_in_should_see_dashboard()
    {
        //here we acted as user and accessed home page 
        $this->actingAs(factory(User::class)->create());
        $response=$this->get('/login')->assertRedirect('/home');
    }

    /** @test */
    public function test_to_check_validation_of_register_page_going_valid()
    {
        $response= $this->post('/register',$this->data());
        $this->assertCount(1,UsersData::all());
    }
    /** @test */
    public function a_name_is_required()
    {
        $response= $this->post('/register',array_merge($this->data(),['name'=>'']));
        $response->assertSessionHasErrors('name');
        $this->assertCount(0,UsersData::all());
       
    }

    /** @test */
    public function test_to_check_whether_user_is_redirecting_login_after_logout()
    {
        $response=$this->get('/logout');
        $response->assertRedirect('/login');

    }
    
    private function data()
    {
        return [
            'name'=>'nithish',
            'email'=>'sainithishsingh14312@gmail.com',
            'password'=>'Nithishsingh@143',
            'password_confirmation'=>'Nithishsingh@143',
            'firstname'=>'saihjhfh',
            'middlename'=>'nithish',
            'lastname'=>'singh',
            'date_of_birth'=>'1999-02-09',
            'address'=>'gajuvaka road no 2',
            'state'=>'andhrapradesh',
            'district'=>'nellore',
            'city'=>'kavali',
            'pincode'=>'524201'
        ];
    }

}
